Découvrez `ZFS`: un stockage fiable, puissant et accessible
===========================================================

[![openzfs logo](img/255-openzfs.png)](https://openzfs.org) [![logo PyConFr](img/255-pyconfr-23-square.png)](https://www.pycon.fr/2023/) [![logo Very Tech Trip 2023](img/255-vtt-2023-square.png)](https://verytechtrip.com/)

`ZFS` est un outil passionnant qui va au-delà d'un système de fichier. Cette présentation se veut techniquement accessible et vise à partager ma découverte de cet outil pour (peut-être) vous donner envie de l'essayer.

Né au début des années 2000 au sein de _Sun Microsytems_, ZFS est aujourd'hui développé au travers du projet [openZFS](https://openzfs.org) pour les noyaux _Linux_ et _freeBSD_.


Support de présentation
-----------------------

* 📺 [`marp`](https://marp.app/) : [`PITCHME.md`](PITCHME.md) / [`PITCHME-en.md`](PITCHME-en.md)
* 📝 2022 [JDLL](https://jdll.org/) PDF : [`pdf/20220402-jdll-openzfs.pdf`](pdf/20220402-jdll-openzfs.pdf)
* 📝 2023 [VTT](https://verytechtrip.com/) PDF : [🇫🇷](pdf/20230203-vtt-openzfs-fr.pdf) / [🇬🇧](pdf/20230203-vtt-openzfs-en.pdf)
* 📝 2023 [PyConFr](https://pycon.fr/2023/) PDF : [🇫🇷](pdf/20230218-pyconfr-openzfs-fr.pdf) / [🇬🇧](pdf/20230218-pyconfr-openzfs-en.pdf)
